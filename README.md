# A Methodological Note on the Creation of the Chatbot “claudIA”: Academic Researcher and Future President

Welcome to the repository for the development and functionality of "claudIA: Academic Researcher and Future President." This innovative generative AI chatbot is modeled after Claudia Sheinbaum, the first female president of Mexico. Utilizing the advanced capabilities of OpenAI's GPT-4, the chatbot is designed to simulate Sheinbaum's expertise in energy efficiency, sustainability, and environmental policy.

## Overview

The chatbot "claudIA" is trained on a meticulously curated dataset, including Sheinbaum’s scientific publications and presidential debate transcripts. It delivers expert-level consultations in academic and policy-making contexts, supporting evidence-based policy formation and educational objectives by providing in-depth knowledge on relevant topics. 

By integrating rigorous academic content with the practical insights of a prominent political figure, the chatbot aims to:
- Inspire sustainable policy initiatives.
- Enhance public understanding of complex issues.
- Support educational goals by providing expert consultations.

## Development Philosophy

The development process of this chatbot underscores a commitment to ethical AI use, ensuring that the chatbot preserves the integrity and transparency of Sheinbaum’s professional and academic contributions. This approach guarantees that the chatbot is not only informative but also trustworthy and respectful of the source material.

## Repository Contents

This repository includes the following components:

1. **Methodological Note**: A detailed explanation of the methodologies employed in the creation and training of the chatbot. [Read the Methodological Note](https://gitlab.com/rduran78/chatbot-claudia/-/blob/main/Methodological%20Note/240606_A_Methodological_Note_on_the_Creation_of_the_Chatbot_Caludia_vff.pdf)
2. **Bot Configuration**: Technical configurations and settings used for the chatbot’s deployment. [Read the Configuration (in Spanish)](https://gitlab.com/rduran78/chatbot-claudia/-/blob/main/Methodological%20Note/Configuracion_ESP.pdf?ref_type=heads)
3. **Training Documents**: The collection of documents and datasets used to train the chatbot, ensuring its responses are accurate and reflective of Sheinbaum's expertise. [View the Training Documents](https://gitlab.com/rduran78/chatbot-claudia/-/tree/main/Training%20Documents?ref_type=heads)

## Usage

The chatbot can be employed in various academic and policy-making contexts to provide insights and consultations on energy efficiency, sustainability, and environmental policy. It is designed to be a valuable resource for researchers, educators, and policymakers seeking expert knowledge in these fields.

**Note**: To run the chatbot, you need access to ChatGPT-4 or ChatGPT-4o.

## Ethical Considerations

In developing "claudIA," we have adhered to strict ethical guidelines to ensure the responsible use of AI. This includes maintaining the accuracy and integrity of the information provided and respecting the intellectual property of the sources used in training the chatbot.

## Citation

Duran-Fernandez, R., Meza, N., & Lugo, M. (2024). *A methodological note on the creation of the chatbot “Claudia”: Academic researcher and future president*. Tecnologico de Monterrey. Retrieved from https://gitlab.com/rduran78/chatbot-claudia

## Contact and Comments

Roberto Duran-Fernandez, Ph.D.  
[roberto_duran@tec.mx](mailto:roberto_duran@tec.mx)

## Access the Chatbot

We invite you to explore the repository and leverage the capabilities of "Claudia" in your academic and policy-making endeavors. For any questions or contributions, please refer to the contributing guidelines or contact the repository maintainers.

---

### **_You can interact with the chatbot "claudIA: Academic Researcher and Future President" by following this link: [Claudia Chatbot](https://chatgpt.com/g/g-JYY7RnUwL-claudia-investigadora-y-futura-presidenta)_**

